#!/bin/bash -e

echo "Janitor configuration"
echo "" > /etc/crontabs/root

echo "Adding ${CROND_TEMPLATE} /usr/local/bin/prune-and-reboot.sh to /etc/crontabs/root"
echo "${CROND_TEMPLATE} /usr/local/bin/prune-and-reboot.sh" >> /etc/crontabs/root

if [[ -n "${EXTRA_COMMAND}" && -n "${EXTRA_COMMAND_CROND_TEMPLATE}" ]];then
  echo "Adding ${EXTRA_COMMAND_CROND_TEMPLATE} bash -c '${EXTRA_COMMAND}' to /etc/crontabs/root"
  echo "${EXTRA_COMMAND_CROND_TEMPLATE} bash -c '${EXTRA_COMMAND}'" >> /etc/crontabs/root
fi

echo "Starting crond"
crond -f
